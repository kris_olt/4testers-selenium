import random
import string

from selenium.webdriver.common.by import By

from fixtures.chrome import chrome_browser
from fixtures.testarena.login import browser
from pages.home_page import HomePage


def generate_random_string(length):
    characters = string.ascii_letters
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string


def generate_random_number():
    return random.randint(150000, 9999999)


def test_e2e_project_creation(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    random_text = generate_random_string(3)
    random_number = generate_random_number()
    project_name = f"MY PROJECT NUMBER {random_number}"
    project_prefix = f"WEB{random_text}".upper()
    project_description = f"Description of project {random_number}"
    browser.find_element(By.CSS_SELECTOR, 'a[title="Administracja"]').click()
    browser.find_element(By.CSS_SELECTOR, 'a.button_link[href*="add_project"]').click()
    browser.find_element(By.CSS_SELECTOR, 'input#name').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, 'input#prefix').send_keys(project_prefix)
    browser.find_element(By.CSS_SELECTOR, 'textarea#description').send_keys(project_description)
    browser.find_element(By.CSS_SELECTOR, 'input#save').click()
    browser.find_element(By.CSS_SELECTOR, '.icon_puzzle_alt.icon-20').click()
    browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
    title_in_search = (By.CSS_SELECTOR, '.article_in_content tbody tr td a')
    title_element = browser.find_element(*title_in_search)
    title_text = title_element.text
    assert title_text == project_name
